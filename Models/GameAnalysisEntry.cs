﻿
using System;
using System.Collections.Generic;


namespace TalkingAnimalsBackend.Models
{
    /// <summary>
    /// Game Export Model: All player data for csv export
    /// </summary>
    public class GameAnalysisEntry
    {
        public int Id { get; set; } 
        public int PlayerId { get; set; }
        public string BraceletId { get; set; }
        public string Language { get; set; }
        public string Name { get; set; }
        public string Team { get; set; }
        public DateTime Time { get; set; }
    }


}
