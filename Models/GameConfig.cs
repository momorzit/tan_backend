﻿namespace TalkingAnimalsBackend.Models 
{ 
    public class GameConfig
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool PlayerChosen { get; set; }
        public bool LongVersion { get; set; }
        public bool IndependentAnimalActivation { get; set; }
        public string DefaultTeam { get; set; }
        public int AudioRepetitions { get; set; }
        public int ResetTime { get; set; }
    }
}
