﻿namespace TalkingAnimalsBackend.Models 
{ 
    public class Player
    {
        public int Id { get; set; }
        public string BraceletId { get; set; }
        public string Language { get; set; }
        public IEnumerable<QuestState> QuestStates { get; set; }
        public string? Team { get; set; }
        public bool IsLongVersion { get; set; }
        public bool IsActive { get; set; }
    }
}
