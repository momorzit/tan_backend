﻿namespace TalkingAnimalsBackend.Models
{
    public class Quest
    {
        public int Id { get; set; }
        // this needs to be true for only one quest, for the team selection
        public bool StartingQuest { get; set; }
        // if the quest is linear it has to be numbered eg.: all stations have to be accessed in the right order
        public bool? IsNumbered { get; set; }
        // the title has currently no inherent use other thanmaking it easier to maintain
        public string Title { get; set; }
        public IEnumerable<Station> Stations { get; set; }
    }
}
