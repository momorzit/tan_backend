﻿namespace TalkingAnimalsBackend.Models 
{ 
    public class EndResult
    {
        public string Language { get; set; }
        public string Team { get; set; }
        public IEnumerable<string> Animals { get; set; }
    }
}
