﻿namespace TalkingAnimalsBackend.Models
{
    public class Station
    {
        // set by the system
        public int Id { get; set; }
        // automatically set when you set the Quest
        public int QuestId { get; set; }
        // if the quest is linear, each station needs to have a number (listeningstation needs to be "0")
        public int Number { get; set; }
        // this is a reference to the quest that contains this station
        public Quest Quest { get; set; }
        // the Name of a station is crucial and has to be unique since it is used to generate the audio file ids
        public string Name { get; set; }
        // this is used to set a players team --> if TeamSelection has a value and the player connects with this station, he will join this particular team
        public string TeamSelection { get; set; }
        // we sometimes need to know whether a station is a listening station or not
        public bool ListeningStation { get; set; }

    }
}
