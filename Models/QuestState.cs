﻿namespace TalkingAnimalsBackend.Models
{
    public class QuestState
    {

        public int Id { get; set; }
        // has the listening station been activated already --> if so the quest counts as active
        public bool IsActive { get; set; }
        public IEnumerable<StationState> StationStates { get; set;}
        // reference to the quest
        public int QuestId { get; set; }
        public Quest Quest { get; set; }
        // reference to the player 
        public int PlayerId { get; set; }
        public Player Player { get; set; }
        // have all stations of this quest been activated -- only really important if this is a linear quest
        public bool Completed { get; set; }
    }
}
