﻿namespace TalkingAnimalsBackend.Models
{
    public class StationState
    {
        public int Id { get; set; } 
        // has been activated correctly --> this is only true if it has been activated while the according quest was active/in the right order
        public bool HasBeenActivated { get; set; }
        public int StationId { get; set; }
        public Station Station { get; set; }
        public int QuestStateId { get; set; }
        public QuestState QuestState { get; set; }
        public int PlayerId { get; set; }
        public Player Player { get; set; }
        public int Activations { get; set; }
        public DateTime? Time { get; set; }
    }
}
