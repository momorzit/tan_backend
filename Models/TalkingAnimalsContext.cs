using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Diagnostics.CodeAnalysis;

namespace TalkingAnimalsBackend.Models
{
    public class TalkingAnimalsContext : DbContext
    {
        public TalkingAnimalsContext(DbContextOptions<TalkingAnimalsContext> options)
            : base(options)
        {

        }
        public DbSet<Player> players { get; set; }
        public DbSet<Quest> quests { get; set; }
        public DbSet<QuestState> questStates { get; set; }
        public DbSet<Station> stations { get; set; }
        public DbSet<StationState> stationStates { get; set; }
        public DbSet<GameConfig> gameConfigs { get; set; }
        public DbSet<GameAnalysisEntry> gameAnalysisEntries { get; set; }

        public IEnumerable<GameConfig> GetConfigs(int configId)
        {
            return gameConfigs;
        }
        public Player GetPlayer(string braceletId)
        {
            return players.Include(p => p.QuestStates)
                .Include(p => p.QuestStates).ThenInclude(qs => qs.StationStates)
                .ThenInclude(st => st.Station)
                .Include(p => p.QuestStates).ThenInclude(qs => qs.Quest)
                .ThenInclude(q => q.Stations).FirstOrDefault(p => p.BraceletId == braceletId);
        }
        public IEnumerable<Player> GetPlayers(bool withStations)
        {
          return players;
        }
        public GameAnalysisEntry GetGAEntry(int playerId, string stationName)
        {
            gameAnalysisEntries.Add( new GameAnalysisEntry { Name = stationName, PlayerId = playerId });
            SaveChanges();
            return gameAnalysisEntries.FirstOrDefault(gae => gae.PlayerId == playerId && gae.Name == stationName);
        }
        public void AddGAEntry(GameAnalysisEntry gae)
        {
            gameAnalysisEntries.Add(gae);
        }
        public IEnumerable<GameAnalysisEntry> GetGAEntries (bool all)
        {
            return gameAnalysisEntries;
        }
        public Player GetPlayerSimple(string braceletId)
        {
            return players.FirstOrDefault(p => p.BraceletId == braceletId);
        }
        public void AddPlayer(Player player)
        {
            players.Add(player);
        }
        public Quest GetQuest(string stationId)
        {
            return quests.Include(q => q.Stations).FirstOrDefault(q => q.Stations.FirstOrDefault(s => s.Name == stationId)!=null);
        }
        public IEnumerable<Quest> GetQuests()
        {
            return quests.Include(q => q.Stations);
        }
        public IEnumerable<Station> GetStations()
        {
            return stations;
        }
        public Station GetStation(string name)
        {
            return stations.FirstOrDefault(s => s.Name.ToLower() == name);
        }
        public IEnumerable<StationState> GetStatStates(int playerId)
        {
            return stationStates.Include(s => s.Station).Where(s => s.PlayerId == playerId);
        }
        public IEnumerable<StationState> GetStatStates(int playerId, int questStateId)
        {
            return stationStates.Include(s => s.Station).Where(s => s.PlayerId == playerId && s.QuestStateId == questStateId);
        }
        // This should be merged into a join and the count should be run on db (so no for each) did not have the time to do/test the db approach
        public IEnumerable<StationState> GetCompletedStations(int playerId)
        {
            return GetStatStates(playerId).Where(s => !s.Station.ListeningStation && s.HasBeenActivated);
        }
        //Setting up Test Data to see and work on the the DB Schema
        public void SetupTestData()
        {
            for (int i = 0; i < 5; i++)
            {
                players.Add(new Player()
                {
                    BraceletId = "Player "+i,
                });
                quests.Add(new Quest()
                {
                    Title = "Quest "+i
                });
                stations.Add(new Station()
                {
                    Name = "Station " + i
                });
            }
            SaveChanges();
            foreach(Player player in players)
            {
                foreach(var quest in quests)
                {
                    player.QuestStates.Append(new QuestState()
                    {
                        Quest = quest,
                        IsActive = false
                    });
                }
            }
            SaveChanges();
            foreach (Player player in players)
            {
                player.QuestStates.FirstOrDefault().StationStates = new List<StationState>
                {
                    new StationState()
                    {
                        Station = stations.FirstOrDefault(s => s.Name == "Station 0")
                    },
                    new StationState()
                    {
                        Station = stations.FirstOrDefault(s => s.Name == "Station 1")
                    },
                    new StationState()
                    {
                        Station = stations.FirstOrDefault(s => s.Name == "Station 2")
                    },
                    new StationState()
                    {
                        Station = stations.FirstOrDefault(s => s.Name == "Station 3")
                    },
                    new StationState()
                    {
                        Station = stations.FirstOrDefault(s => s.Name == "Station 4")
                    }
                };
            }
            SaveChanges();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Station>().HasIndex(u => u.Name)
                .IsUnique(); // HasAlternateKey means it can be used as the target of a ForeignKey, which is overkill for constraining uniqueness
            modelBuilder.Entity<Player>().HasIndex(u => u.BraceletId).IsUnique();
            modelBuilder.Entity<QuestState>().HasOne(qs => qs.Quest);
            modelBuilder.Entity<StationState>().HasOne(u => u.Station);
            modelBuilder.Entity<StationState>().HasOne(u => u.Player);


        }
    }
}
