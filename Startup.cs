﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TalkingAnimalsBackend.Models;


namespace SingletonApp.Backend
{

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                   builder.AddFilter((category, level) =>
                    level == LogLevel.Warning || level == LogLevel.Error || level == LogLevel.Critical).AddConsole());
            return serviceCollection.BuildServiceProvider()
                                    .GetService<ILoggerFactory>();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            
            var connectionString = Configuration.GetConnectionString("Production");

            services.AddRouting();
            services.AddDbContextPool<TalkingAnimalsContext>(options => options.EnableSensitiveDataLogging(true).UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                        .AddCookie(o => o.LoginPath = new PathString("/home/login"));
            services.AddControllers();

            services.AddLogging();

        }
        
        
        public class ResponseLogger
        {
            private RequestDelegate next;

            public ResponseLogger(RequestDelegate next)
            {
                this.next = next;
            }

            public async Task Invoke(HttpContext context)
            {
                var originalBody = context.Response.Body;
                using (var newBody = new MemoryStream()) {
                    context.Response.Body = newBody;
                    try
                    {
                        await this.next(context);
                    }
                    finally {
                        newBody.Seek(0, SeekOrigin.Begin);
                        var bodyText = await new StreamReader(context.Response.Body).ReadToEndAsync();
                        Console.WriteLine($"Logging Response Body: {bodyText}");
                        newBody.Seek(0, SeekOrigin.Begin);

                        foreach (var item in context.Response.Headers) {
                            Console.WriteLine("Logger Headers, key:  " + item.Key);
                            Console.WriteLine("Logger Headers, value: " + item.Value);
                        }
                        await newBody.CopyToAsync(originalBody);
                    }    
                }
            }
        }
        
        public class LogExceptions
        {
            private readonly RequestDelegate _next;
            private readonly ILogger<LogExceptions> _logger;

            public LogExceptions(RequestDelegate next, ILogger<LogExceptions> logger)
            {
                _logger = logger;
                _next = next;
            }

            public async Task InvokeAsync(HttpContext httpContext)
            {
                try
                {
                    // next request in pipeline
                    await _next(httpContext);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Exception risen: {ex}");
                    await HandleExceptionAsync(httpContext, ex);
                }
            }

            /// <summary>
            /// Handle runtime error
            /// </summary>
            /// <param name="context"></param>
            /// <param name="exception"></param>
            /// <returns></returns>
            private Task HandleExceptionAsync(HttpContext context, Exception exception)
            {
                // Customise it to handle more complex errors
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                return context.Response.WriteAsync($"Something went wrong: {exception.Message}");
            }
        }
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStaticFiles();
            app.UseDeveloperExceptionPage();
            app.UseRouting();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            app.UseAuthentication();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });

            if (env.IsDevelopment())
            {
                app.Use(async (context, next) =>
                {
                    await next.Invoke();
                });
                
                app.UseMiddleware<ResponseLogger>();
                app.UseMiddleware<LogExceptions>();

            }

        }
    }
}
