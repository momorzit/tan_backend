﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TalkingAnimalsBackend.Models;

namespace TalkingAnimalsBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameConfigController : ControllerBase
    {
        [HttpGet("all")]
        public  List<GameConfig>GetAllConfigs()
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            return context.GetConfigs(1).ToList();
        }
        [HttpGet("activate_config/{config_name}")]
        public GameConfig ActivateConfig(string config_name)
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            var newActiveConfig = context.GetConfigs(1).FirstOrDefault(gc => gc.Name == config_name);
            foreach(GameConfig conf in context.GetConfigs(1).Where(gc => gc.Active).ToList())
            {
                if(conf != newActiveConfig)
                {
                    conf.Active = false;
                }
            }
            newActiveConfig.Active = true;
            context.SaveChanges();
            return context.GetConfigs(1).FirstOrDefault(gc => gc.Active);
        }
    }
}
