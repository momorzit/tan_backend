﻿using System.Globalization;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TalkingAnimalsBackend.Models;


namespace TalkingAnimalsBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class GameAnalysisController : Controller
    {
        [HttpGet("all")]
        public IEnumerable<GameAnalysisEntry> GetRaw()
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            return context.GetGAEntries(true);
        }
        [HttpGet("csv")]
        public FileContentResult GetCsv()
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            IEnumerable<GameAnalysisEntry> exportData = context.GetGAEntries(true);

            var memoryStream = new MemoryStream();
            using (var writer = new StreamWriter(memoryStream))
            {
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.Delimiter = ";";
                    csv.WriteRecords(exportData);
                    writer.Flush();
                    return File(memoryStream.ToArray(), "text/csv", "game_export_" + DateTime.Now.Date.ToString() + ".csv");
                }

            }
        }
    }
}
