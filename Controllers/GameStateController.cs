using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TalkingAnimalsBackend.Models;

namespace TalkingAnimalsBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class GameStateController : Controller
    {
        [HttpGet("create_data")]
        public void CreateMockUpData()
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            context.SetupTestData();
        }
        [HttpGet("test/{stationId}/{braceletId}")]
        public ActionResult<int> TestIds(string stationId, string braceletId)
        {
            if (!String.IsNullOrEmpty(stationId) && !String.IsNullOrEmpty(braceletId))
            {
                return 0;
            }
            else if (String.IsNullOrEmpty(stationId))
            {
                return 1;
            }
            else if (String.IsNullOrEmpty(braceletId))
            {
                return 2;
            }
            else
                return 3;
        }

        [HttpGet("addnewplayer/{language}/{bracelet_id}")]
        public ActionResult<string> AddNewPlayer(string bracelet_id, string language)
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            if (bracelet_id != null)
            {
                var currentPlayer = context.GetPlayerSimple(bracelet_id);
                if (currentPlayer != null)
                {
                    currentPlayer.BraceletId = DateTime.UtcNow.ToString();
                    currentPlayer.IsActive = false;
                    //return "player with bracelet id " + bracelet_id + " already exists";
                    context.SaveChanges();
                }
                string lang = "";
                if (language == "english")
                {
                    lang = "EN";
                }
                else if (language == "german")
                {
                    lang = "DE";
                }
                var newPlayer = new Player
                {
                    BraceletId = bracelet_id,
                    Language = lang,
                    IsLongVersion = false,
                    IsActive = true
                };
                context.AddPlayer(newPlayer);
                //Save Player before adding station/queststates
                context.SaveChanges();
                var quests = context.GetQuests();
                List<QuestState> queststates = new List<QuestState>();
                List<StationState> stationstates = new List<StationState>();
                foreach (Quest quest in quests)
                {
                    foreach (Station station in quest.Stations)
                    {
                        stationstates.Add(new StationState
                        {
                            Player = newPlayer,
                            Station = station,
                            HasBeenActivated = false,
                            Activations = 0
                        });
                    }
                    var curQuestState = new QuestState
                    {
                        Player = newPlayer,
                        StationStates = stationstates,
                        Completed = false,
                        IsActive = false,
                        Quest = quest
                        
                    };
                    //setting the starting quest to active
                    if(quest.StartingQuest)
                    {
                        curQuestState.IsActive = true;
                    }
                    queststates.Add(curQuestState);
                    stationstates = new List<StationState>();
                }
                newPlayer.QuestStates = queststates;
                context.SaveChanges();
                return "created new player for bracelet id: " + bracelet_id;
            }
            return "no bracelet id given";
        }
        [HttpGet("jointeam/{teamName}/{braceletId}")]
        public ActionResult<string> JoinTeam(string teamName, string braceletId)
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            var currentPlayer = ResetPlayer(currentPlayer.BraceletId);
            var statState = context.GetStatStates(currentPlayer.Id).FirstOrDefault(s => s.Station.Name == "Spielstart");
            var lastActivation = context.GetStatStates(currentPlayer.Id).Max(s => s.Time);
            var now = DateTime.UtcNow;
            teamName = teamName.ToLower();
            var curConf = context.GetConfigs(0).FirstOrDefault(gc => gc.Active);
            var resetTime = curConf == null ? 90 : curConf.ResetTime;

            string result = currentPlayer.Language + "_QuestDescription_Spielstart_";
            if (teamName == "sport")
            {
                currentPlayer.Team = "Sport";
                result = result + "Sport";
            }
            else if (teamName == "musik")
            {
                currentPlayer.Team = "Musik";
                result = result + "Musik";
            }
            else if (teamName == "kochen")
            {
                currentPlayer.Team = "Kochen";
                result = result + "Kochen";
            }
            else
            {
                statState.Time = DateTime.UtcNow;
                statState.Activations += 1;
                var cGAEntry = new GameAnalysisEntry { PlayerId = currentPlayer.Id, Name = statState.Station.Name, Language = currentPlayer.Language };
                cGAEntry.Team = currentPlayer.Team==null?"": currentPlayer.Team;
                cGAEntry.Name = "Spielstart";
                cGAEntry.BraceletId = braceletId;
                cGAEntry.Time = now;
                context.AddGAEntry(cGAEntry);
                context.SaveChanges();
                return currentPlayer.Language + "_QuestDescriptionShort_Start";
            }
            var currentQuestState = currentPlayer.QuestStates.FirstOrDefault(q => q.Quest.StartingQuest);
            currentQuestState.Completed = true;
            var curGAEntry = new GameAnalysisEntry { PlayerId = currentPlayer.Id, Name = statState.Station.Name, Language = currentPlayer.Language };
            curGAEntry.Team = currentPlayer.Team;
            curGAEntry.Name = "Spielstart";
            curGAEntry.BraceletId = braceletId;
            curGAEntry.Time = now;
            context.AddGAEntry(curGAEntry);
            context.SaveChanges();
            return result;
        }
            //This is the main Logic handling the responses
        [HttpGet("getaudioid/{stationId}/{braceletId}")]
        public ActionResult<string> GetAudioId(string stationId, string braceletId)
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            var curConf = context.GetConfigs(1).FirstOrDefault(gc => gc.Active);
            var now = DateTime.UtcNow;
            if (curConf == null) return "config_does_not_exist";
            var currentPlayer = ResetPlayer(braceletId);
            if (currentPlayer != null)
            {
                Quest currentQuest = context.GetQuest(stationId);
                if (currentQuest != null)
                {
                    Station currentStation = context.GetStation(stationId);
                    if (currentStation == null) return "station_does_not_exist";
                    var curGAEntry = new GameAnalysisEntry { PlayerId = currentPlayer.Id, Name = stationId };
                    curGAEntry.Language = currentPlayer.Language;
                    curGAEntry.Team = currentPlayer.Team;
                    curGAEntry.BraceletId = braceletId;

                    if (currentStation.Name == "Ausgabe" || currentStation.Name == "Abgabe")
                    {
                        var curStatState = currentPlayer.QuestStates.FirstOrDefault(qs => qs.Quest.Id == currentQuest.Id).StationStates.FirstOrDefault(s => s.Station == currentStation);
                        curStatState.Activations += 1;
                        curStatState.Time = now;
                        curGAEntry.Time = now;
                        var curGAEntry = new GameAnalysisEntry { PlayerId = currentPlayer.Id, Name = stationId };
                        curGAEntry.Language = currentPlayer.Language;
                        curGAEntry.Team = currentPlayer.Team;
                        curGAEntry.BraceletId = braceletId;
                        context.SaveChanges();
                        if (currentStation.Name == "Ausgabe")
                        {
                            return currentPlayer.Language + "_QuestDescription_Ausgabe_all";
                        }
                        if (currentStation.Name == "Abgabe")
                        {
                            return currentPlayer.Language + "_QuestDescription_Abgabe_all";
                        }
                    }
                    if (!currentPlayer.QuestStates.FirstOrDefault(qs => qs.Quest.StartingQuest).Completed)
                    {
                        currentPlayer.Team = curConf.DefaultTeam;
                        curGAEntry.Team = currentPlayer.Team;
                        context.SaveChanges();
                    }
                    var currentQuestState = currentPlayer.QuestStates.FirstOrDefault(qs => qs.Quest.Id == currentQuest.Id);
                    if (currentQuestState == null) return "queststate_does_not_exist";

                    //Checking if the the quest(state) that is connected to the current station is active, or if it is a a Listening station which would activate the the Quest(State)
                    if (currentQuestState.IsActive || currentStation.ListeningStation || curConf.IndependentAnimalActivation)
                    {
                        var curStatState = currentQuestState.StationStates.FirstOrDefault(s => s.Station == currentStation);
                        if (curStatState != null)
                        {
                            curStatState.Activations += 1;
                            curStatState.Time = now;
                            curGAEntry.Time = now;
                            context.AddGAEntry(curGAEntry);
                            context.SaveChanges();

                            if (curStatState.Activations > 1 && currentStation.ListeningStation)
                            {
                                return currentPlayer.Language + "_QuestShortRepetition_" + currentStation.Name + "_all";
                            }
                            if ((curStatState.Activations > curConf.AudioRepetitions || currentQuestState.Completed) && currentStation.ListeningStation)
                            {
                                return currentPlayer.Language + "_QuestYouHaveBeenHere_" + currentStation.Name + "_all";
                            }
                            if (currentStation.ListeningStation)
                            {
                                currentQuestState.IsActive = true;
                                context.SaveChanges();
                            }
                            if (!curStatState.HasBeenActivated)
                            {
                                curStatState.HasBeenActivated = true;
                                //setting the quest state to active (if it is a listening station, we need to "start" the quest)
                                if (currentStation.ListeningStation)
                                {
                                    if (currentPlayer.IsLongVersion || (curConf.LongVersion && !curConf.PlayerChosen))
                                    {
                                        if (currentStation.Name == "Ankommen" || currentStation.Name == "Spielstart")
                                        {
                                            return currentPlayer.Language + "_QuestDescription_" + currentStation.Name + "_" + currentPlayer.Team;
                                        }
                                        return currentPlayer.Language + "_QuestDescription_" + currentStation.Name + "_all";
                                    }//Special cases for two stations are now hardcoded, these two have team dependent audios
                                    if (currentStation.Name == "Ankommen" || currentStation.Name == "Spielstart")
                                    {
                                        return currentPlayer.Language + "_QuestDescriptionShort_" + currentStation.Name + "_" + currentPlayer.Team;
                                    }
                                    return currentPlayer.Language + "_QuestDescriptionShort_" + currentStation.Name;
                                }
                                //setting quests to completed 
                                if (context.GetStatStates(currentPlayer.Id, currentQuestState.Id).All(s => s.HasBeenActivated == true))
                                {
                                    currentQuestState.Completed = true;
                                }
                                context.SaveChanges();
                                if(context.GetStatStates(currentPlayer.Id, currentQuestState.Id).Where(s => !s.Station.ListeningStation && s != curStatState).Any(s => s.HasBeenActivated))
                                {
                                    return currentPlayer.Language + "_CorrectAnimalActivated_" + currentStation.Name + "_" + currentPlayer.Team + "_nohint";
                                }
                                return currentPlayer.Language + "_CorrectAnimalActivated_" + currentStation.Name + "_" + currentPlayer.Team;
                            }
                            if((curStatState.Activations < curConf.AudioRepetitions && curStatState.HasBeenActivated)||(curConf.IndependentAnimalActivation))
                            {
                                return currentPlayer.Language + "_CorrectAnimalActivated_" + currentStation.Name + "_" + currentPlayer.Team;
                            }
                            return currentPlayer.Language + "_IncorrectAnimalActivated_" + currentStation.Name + "_all";
                        }
                    }
                    if(currentStation.Name == "Kojote" && currentQuest.Title == "Ankommen")
                    {
                        return currentPlayer.Language + "_IncorrectAnimalActivated_KojoteVonAnkommen_all";
                    }
                    return currentPlayer.Language + "_GoToHeadphones_" + currentStation.Name + "_all";
                }
                return "quest_does_not_exist";
            }
            return "player_does_not_exist";
        }
        [HttpGet("end/{bracelet_id}")]
        public ActionResult<EndResult> GetEndResult(string bracelet_id)
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            var currentPlayer = ResetPlayer(bracelet_id);
            var curGAEntry = new GameAnalysisEntry { PlayerId = currentPlayer.Id};
            var now = DateTime.UtcNow;
            curGAEntry.Team = currentPlayer.Team == null ? "" : currentPlayer.Team;
            curGAEntry.Language = currentPlayer.Language == null ? "" :currentPlayer.Language;
            curGAEntry.Name = "Endstation";
            curGAEntry.BraceletId = bracelet_id;
            curGAEntry.Time = now;
            context.AddGAEntry(curGAEntry);
            context.SaveChanges();
            var result = new EndResult();
            result.Language = currentPlayer.Language;
            result.Team = currentPlayer.Team;
            if (result.Team == null || context.GetCompletedStations(currentPlayer.Id).Count() < 3)
            {
                return result;
            }
            result.Animals = context.GetCompletedStations(currentPlayer.Id).Select(s => s.Station.Name).ToList();
            return result;
        }
        [HttpGet("endtest/{bracelet_id}")]
        public ActionResult<EndResult> GetEndResultTest(string bracelet_id)
        {
            var result = new EndResult();
            var rand = new Random();
            var bytes = new byte[3]; ;
            rand.NextBytes(bytes);
            var allAnimals = new List<string> (){ "Steinbock","Waschbaer"
                ,"Pfau","Kojote"
                ,"Murmeltier","Siebenschlaefer"
                ,"Schnabeltier","Papagei"
                , "Kragenechse","Zebra"};
            result.Animals = new List<string>();
            if (bytes[0] % 3 == 1)
            {
                result.Team = "Sport";
            }
            else if (bytes[0] % 3 == 0)
            {
                result.Team = "Kochen";
            }
            else
            {
                result.Team = "Musik";
            }
            if (bytes[0] % 2 == 0)
            {
                result.Language = "DE";
            }
            else { result.Language = "EN"; }
            result.Animals = allAnimals;
            return result;
        }
        //auxiliary method to reset the player data to an empty state
        public Player ResetPlayer(string braceletId)
        {
            TalkingAnimalsContext context = HttpContext.RequestServices.GetService(typeof(TalkingAnimalsContext)) as TalkingAnimalsContext;
            var currentPlayer = context.GetPlayer(braceletId);
            var firstActivation = context.GetStatStates(currentPlayer.Id).Min(s => s.Time);
            if (firstActivation != null && ((TimeSpan)((DateTime.UtcNow - firstActivation))).Minutes > 90)
            {
                currentPlayer.BraceletId = DateTime.UtcNow.ToString();
                currentPlayer.IsActive = false;
                //return "player with bracelet id " + bracelet_id + " already exists";
                context.SaveChanges();
                var newPlayer = new Player
                {
                    BraceletId = braceletId,
                    Language = currentPlayer.Language,
                    IsLongVersion = false,
                    IsActive = true
                };
                context.AddPlayer(newPlayer);
                //Save Player before adding station/queststates
                context.SaveChanges();
                var quests = context.GetQuests();
                List<QuestState> queststates = new List<QuestState>();
                List<StationState> stationstates = new List<StationState>();
                foreach (Quest quest in quests)
                {
                    foreach (Station station in quest.Stations)
                    {
                        stationstates.Add(new StationState
                        {
                        Player = newPlayer,
                        Station = station,
                        HasBeenActivated = false,
                        Activations = 0
                        });
                    }
                    var curQuestState = new QuestState
                    {
                        Player = newPlayer,
                        StationStates = stationstates,
                        Completed = false,
                        IsActive = false,
                        Quest = quest

                    };
                    //setting the starting quest to active
                    if (quest.StartingQuest)
                    {
                        curQuestState.IsActive = true;
                    }
                    queststates.Add(curQuestState);
                    stationstates = new List<StationState>();
                }
                newPlayer.QuestStates = queststates;
                context.SaveChanges();
                return newPlayer;
            }
            return currentPlayer;
        }
    }
}
