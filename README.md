# tan_backend

talking animals backend app

# how to use it

- create quests and stations in the database 
- queststates + stationstates will be created when setting up a user (there is an endpoint for it)
- each user will have queststates and each of these queststates will ahve stationtstates 
- the states contain the progress data of the user
- when accessing a station the backend receives the name of the station and the unique id of the players bracelet
- the endpoint uses this to generate the name of the next track and sends it to the station which will play it accordingly

# important notes

- name and password of the database is contained in the "appsettings.json" file (in case you need to look it up/change it)
- object ids are generally set automatically and ARE NOT TO BE CHANGED by any kind of logic
- Stations contain the Field TeamSelection --> if there is a value in this field, this station will set the players team to this value (you can change teams with this)

# setup (on a do droplet)
https://www.digitalocean.com/community/tutorials/how-to-deploy-an-asp-net-core-application-with-mysql-server-using-nginx-on-ubuntu-18-04
- Just follow the instructions but make sure to install the dotnet 6.0 environment instead of the 2.2 (replacing 2.2 with 6.0 in the commands will literally do the trick)
- you can download the necessary files with "git clone https://gitlab.com/momorzit/tan_backend.git" into /var/tan_backend
- after that you can build/publish the app with the previously installed dotnet cli (explained in the tutorial)
- db scheme can be taken from the test server (46.101.185.47 password for the admin is "basteln" same as root) <-- PW NEEDS TO BE CHANGED IF THIS IS ONLINE and not in a local env
- create the follwoing .service file with the command "sudo nano etc/systemd/system/tan_backend.service":
	[Unit]
	Description=Talking Animals Backend

	[Service]
	WorkingDirectory=/var/tan_backend
	ExecStart=/usr/bin/dotnet /var/tan_backend/bin/Debug/net6.0/TalkingAnimalsBackend.dll
	Restart=always
	RestartSec=10
	SyslogIdentifier=tan_backend
	User=root
	Environment=ASPNETCORE_ENVIRONMENT=Production
	Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false
	
	[Install]
	WantedBy=multi-user.target
- replace the default sites available ("sudo nano /etc/nginx/sites-available/default") with the following:
	server {
		listen 80;
		location / {
			proxy_pass http://localhost:5123;
			proxy_http_version 1.1;
			proxy_set_header Upgrade $http_upgrade;
			proxy_set_header Connection keep-alive;
			proxy_set_header Host $host;
			proxy_cache_bypass $http_upgrade;

			if ($request_method = 'POST') {
				add_header 'Access-Control-Allow-Origin' '*';
				add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
				add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
				add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
			}
			if ($request_method = 'GET') {
				add_header 'Access-Control-Allow-Origin' '*';
				add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
				add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
				add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
			}
		}
	}






